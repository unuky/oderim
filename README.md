# Leeme #


Esto es un repositorio usado para el gamemode llamado Oderim, una base que puede usar para un nuevo proyecto en SAMP.

### ¿Qué necesita? ###

* Servidor SAMP
* Include gl-common
* Include dini de Dracoblue
* Carpeta "Usuarios" en scriptfiles

### Sobre la licencia ###

Oderim by unuky is licensed under a [Creative Commons Reconocimiento-NoComercial 4.0 Internacional License](http://creativecommons.org/licenses/by-nc/4.0/).